# coding: utf-8

'''
Projet N°1 de NSI : Yam's
Créé par Nil Superchi, Mathieu Ducreux et Alexandre Fortin

Règle du jeu :
Le Yams se joue avec 5 dés et se finit une fois toutes les cases de la fiche de score remplies. Chaque joueur joue tour 
à tour et dispose de 3 lancers à chaque coup. L’objectif étant de réaliser des combinaisons qui rapportent des points. 
Le joueur a le choix de reprendre tous ou une partie des dés à chaque lancé, selon son gré, pour tenter d’obtenir la 
combinaison voulue. A chaque tour, le joueur doit obligatoirement inscrire son score dans une des cases de la feuille 
de marque que ce soit par un X ou par les points qu’il a obtenu.

Il peut arriver lors d’un tour que le résultat ne convienne pas au joueur et qu’il se dise qu’il pourrait faire un plus 
grand score sur un autre tour. Il peut alors choisir de barrer une autre case à la place. Bien entendu, il ne pourra plus 
faire cette combinaison par la suite.

Lorsque le total intermédiaire est égal ou supérieur à 63 points, un bonus de 35 points supplémentaires est accordé, 
ce qui peut faire la différence au décompte final. Soyez donc stratégique !.

Le gagnant d’une partie de Yams est le joueur qui comptabilisera le plus de points à la fin des 10 coups.
'''

from random import randint
from tkinter import *
from tkinter.messagebox import showinfo

fenetre = Tk()
resultat_dict = {}
resultat_tab = []

label = Label(fenetre, text="Yam's")
label.pack()

scrollbar = Scrollbar(fenetre)
scrollbar.pack( side = RIGHT, fill = Y )

liste = Listbox(fenetre, height=50, width=80, yscrollcommand=scrollbar.set)

def lancer_des():
    '''
    Cette fonction permet de lancer les dés aléatoirement 
    Sortie: tableau des résultats des dés lancés
    '''
    liste_aleatoire = [randint(1, 6) for _ in range(5)]
    return(liste_aleatoire)


def calculer_moyenne():
    '''
    Cette fonction permet de calculer la moyenne des points
    Sortie: moyenne (message afficher dans un show info)
    '''
    somme = 0
    for score in resultat_dict.values():
           somme += score
    moyenne = somme / len(resultat_dict)
    moyenne = round(moyenne, 2)
    showinfo(title='Information', message=f'La moyenne des scores du jeu est de : {moyenne} ' )
    return moyenne


def jouer_tour(pcharacters_tab):
    '''
    Cette fonction permet de jouer un tour
    Entrée: tableau des noms des joueurs
    '''

    tirage_tab = []
    num_joueur = 0
    # pour chaque joueur du fichier
    for character in pcharacters_tab:
        score = 0
        compteur = 0
    
        # on lance les dès
        resultat_des = lancer_des()
        tirage_dict = {}
    
        # pour chaque face des dès on compte le nb de fois sortie
        for i in range(1, 7):
            tirage_dict[i] = resultat_des.count(i)
        
        # si on veut ajouter le dict dans un tableau
        tirage_tab.append(tirage_dict)
        
        if 5 in tirage_dict.values():
            # on obtient 5 fois la même valeur : yams
            score += 15
        
        elif 5 in tirage_dict.values():
            # on obtient 4 fois la même valeur : carré
            score += 5
            
        elif tirage_dict[1] == 1 and tirage_dict[2] == 1 and tirage_dict[3] == 1 and tirage_dict[4] == 1 and tirage_dict[5] == 1:
            # on obtient une suite de 5 dés qui se suivent
            score += 9
            
        elif 3 in tirage_dict.values():
            if 2 in tirage_dict.values():
                # on obtient un full
                score += 7
            else:
                # on obtient un brelan
                score += 3
                
        elif 2 in tirage_dict.values():
            # on a au moins une paire, il faut compter le nb de paire :
            for i in tirage_dict.values():
                if i == 2:
                    compteur = compteur + 1
            if compteur == 2: 
                # on a une double paire
                score += 2
            else:
                # on a qu'une seule paire
                score += 1
        
        # on ajoute le score au score précédent dans le dictionnaire resultat 
        compteur = 0
        for score_prec in resultat_dict.values():
            if compteur == num_joueur:
                score = score + score_prec
            compteur = compteur + 1
        
        # on ajoute le nouveau score dans le dico resultat 
        resultat_dict[character['Name']] = score
        
        num_joueur += 1


def lancer_jeu():
    '''
    Cette fonction permet de lancer le jeu
    '''
    characters_tab = []
    with open("Characters.csv", mode='r', encoding='utf-8') as f:
        lines = f.readlines()
        key_line = lines[0].strip()
        keys = key_line.split(";")
        for line in lines[1:]:
            line = line.strip()
            values = line.split(';')
            dico = {}
            for i in range(1, 2):
                dico[keys[i]] = values[i]
            characters_tab.append(dico)
            
    # initialisation des scores à 0 dans le dico resultat
    resultat_dict = {character['Name'] : 0 for character in characters_tab}
    for j in range(10):
        jouer_tour(characters_tab)
    resultat_tab = list(resultat_dict.items())
    afficher_resultats()
    
    

def afficher_resultats():
    '''
    Cette fonction permet d'afficher les resultats dans une l'IHM
    '''
    compteur = 0
    
    for cle, valeur in resultat_dict.items():      
        ligne = (f'{cle} a obtenu {valeur} points')
        liste.insert(compteur, ligne)
        compteur += 1
    liste.pack()


def tri_resultat(presultat_tab):
    '''
    Cette fonction est censé faire le tris
    
    '''
    presultat_tab = list(resultat_dict.items())
    for k in range(1, len(presultat_tab)):
        while presultat_tab[k] < presultat_tab[k - 1] and k > 0 :
            presultat_tab[k], presultat_tab[k - 1] = presultat_tab[k - 1], presultat_tab[k]
            k = k - 1
    return presultat_tab


def reinitialiser_score():
    '''
    Cette fonction reinitialise les resultats
    '''
    liste.delete(0,END)
    resultat_dict.clear()
    afficher_resultats()


bouton_lancer=Button(fenetre, text="Lancer le jeu", command=lancer_jeu)
bouton_lancer.pack()

bouton_moyenne=Button(fenetre, text="Calculer la moyenne des scores", command=calculer_moyenne)
bouton_moyenne.pack()

bouton_reinit=Button(fenetre, text="Réinitialiser les scores", command=reinitialiser_score)
bouton_reinit.pack()

fenetre.mainloop()

